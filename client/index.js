req()

async function req() {
  console.log('start')
  const data = await request('http://localhost:3000/api/1')
  console.log(data)
}

async function request(url, method = 'GET', date = null) {
  try {
    const headers = {}
    let body

    if (date) {
      headers['Content-Type'] = 'application/json'
      body = JSON.stringify(data)
    }

    const res = await fetch(url, {
      method,
      headers,
      body
    })

    return await res.json()
  } catch (e) {
    console.warn(e.messege)
  }
}

fetch('/api')