const express = require('express')
const app = express()
const path = require('path')

const DATA = [1, 2, 3, 4, 5]

app.get('/api/1', (req, res) => {
  console.log('start')
  res.status(200).json(DATA)
  console.log(DATA)
})



app.use(express.static(path.resolve(__dirname, 'client')))

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'client', 'index.html'))
})

app.listen(3000, () => console.log('server up'))

